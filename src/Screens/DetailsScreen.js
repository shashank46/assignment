/* eslint-disable prettier/prettier */
import React from 'react';
import {Avatar} from 'react-native-elements';
import {ActivityIndicator} from 'react-native';
import {Image} from 'react-native-elements';
import {StyleSheet, Text, View} from 'react-native';

const Detailsscreen = ({route}) => {
  const {item} = route.params;
  return (
    <View style={{alignContent: 'center'}}>
      <Image
        source={{uri: item.avatar}}
        style={{width: 200, height: 200, alignContent: 'center'}}
        PlaceholderContent={<ActivityIndicator />}
      />
      <Text style={{fontSize: 40}}>
        {item.first_name}
        {item.last_name}
      </Text>
    </View>
  );
};

export default Detailsscreen;

const styles = StyleSheet.create({});
