/* eslint-disable prettier/prettier */
import React from 'react';
import {StyleSheet, Text, View, Button} from 'react-native';
import Records from '../Components/Records';

const Homescreen = ({navigation}) => {
  return (
    <View>
      <Text>Homescreen</Text>

      <Records />
    </View>
  );
};

export default Homescreen;

const styles = StyleSheet.create({});
