/* eslint-disable prettier/prettier */

import api from '../Apis';
import axios from 'axios';

const fetchData = pageno => dispatch => {
  const response = api.get(`/api/users?page=${pageno}`).then(data => {
    // console.log(data);
    dispatch({type: 'FETCH_DATA', payload: data.data});
  });
  //   console.log('response', response);
  //   dispatch({type: 'FETCH_DATA', payload: response.data});
};
const postData = data => dispatch => {
  console.log(data);
  dispatch({type: 'POST_DATA', payload: data});

  //   console.log('response', response);
  //   dispatch({type: 'FETCH_DATA', payload: response.data});
};

export const action = {fetchData, postData};
