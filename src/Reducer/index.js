/* eslint-disable prettier/prettier */
import {combineReducers} from 'redux';
import UserReducer from './fetchUserReducer';

export default combineReducers({
  records: UserReducer.reducer,
});
