/* eslint-disable prettier/prettier */
import React, {Component, useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {
  Text,
  View,
  StyleSheet,
  Button,
  FlatList,
  TouchableOpacity,
  Modal,
  ScrollView,
} from 'react-native';
import {action} from '../Action/index';
import {Avatar, ListItem} from 'react-native-elements';
import {SafeAreaView} from 'react-native-safe-area-context';

const Records = ({records, fetchData, navigation}) => {
  const [showInfo, setShowInfo] = useState(false);
  const [pageNo, setPageNo] = useState(1);

  useEffect(() => {
    fetchData(pageNo);
  }, [pageNo, fetchData]);

  const onDetails = item => {
    navigation.navigate('Detail', {item});
    console.log('############################');
  };

  console.log(JSON.stringify(records, null, 4));
  return (
    <SafeAreaView>
      <Button title="Adddata" onPress={() => navigation.navigate('Form')} />
      <Button title="LoadMore" onPress={() => setPageNo(prev => prev + 1)} />

      <Text>I am user Recordscreen</Text>

      <FlatList
        //   horizontal
        //   showsHorizontalScrollIndicator={false}
        keyExtractor={record => record.id}
        data={records}
        renderItem={({item}) => {
          return (
            <View>
              <ListItem>
                <Avatar title={item.name} source={{uri: item.avatar}} />
                <Text>
                  {item.first_name} {item.last_name}
                </Text>
                <TouchableOpacity onPress={() => onDetails(item)}>
                  <Text style={styles.font}>{item.email}</Text>
                </TouchableOpacity>
                {/* <ListItem.Content>
                  <ListItem.Title>{item.name}</ListItem.Title>
                  <ListItem.SubTitle>{item.email}</ListItem.SubTitle>
                </ListItem.Content> */}
              </ListItem>
            </View>
          );
        }}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  font: {
    fontSize: 20,
  },
});
const mapStateToProps = state => {
  // console.log(state.records);
  return {records: state.records};
};
const mapDispatchToProps = dispatch => ({
  fetchData: pageno => dispatch(action.fetchData(pageno)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Records);
