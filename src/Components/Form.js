/* eslint-disable prettier/prettier */
import React, {Component, useState} from 'react';
import {connect} from 'react-redux';
import {Text, View, StyleSheet, TextInput, Button} from 'react-native';
import {action} from '../Action';

const Edit = ({postData, navigation}) => {
  const [email, onChangeEmail] = useState('Useless Text');
  const [userName, onChangeUserName] = useState('');
  const onsubmit = () => {
    console.log(email, userName, 'KKKKKKKKKKKKKK');
    postData([
      {
        id: '5677',
        email: email,
        first_name: userName,
        last_name: 'Ramos',
        avatar: 'https://reqres.in/img/faces/6-image.jpg',
      },
    ]);
    navigation.navigate('Home');
  };
  return (
    <View>
      <TextInput
        style={styles.input}
        onChangeText={onChangeEmail}
        value={email}
      />
      <TextInput
        style={styles.input}
        onChangeText={onChangeUserName}
        value={userName}
      />
      <Button title="Submit" onPress={() => onsubmit()} />
    </View>
  );
};

const styles = StyleSheet.create({});
const mapStateToProps = state => {
  // console.log(state.records);
  return {records: state.records};
};
const mapDispatchToProps = dispatch => ({
  postData: data => dispatch(action.postData(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Edit);
