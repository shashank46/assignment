/* eslint-disable prettier/prettier */
import React from 'react';
import {View, Text} from 'react-native';
import {Provider} from 'react-redux';
import ReduxThunk from 'redux-thunk';
import {createStore, applyMiddleware} from 'redux';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import HomeScreen from '../Screens/Homescreen';
import FormsScreen from '../Screens/FormsScreen';
import Detailsscreen from '../Screens/DetailsScreen';
import combineReducers from '../Reducer/index';
import Records from './Records';
import Form from './Form';

const store = createStore(combineReducers, applyMiddleware(ReduxThunk));
const Stack = createStackNavigator();
function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" component={Records} />
          <Stack.Screen name="Form" component={Form} />
          <Stack.Screen name="Detail" component={Detailsscreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}
export default App;
