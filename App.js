import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Routes from './src/Components/Routes';

const App = () => {
  return <Routes />;
};

export default App;

const styles = StyleSheet.create({});
